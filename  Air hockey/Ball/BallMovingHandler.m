#import <Foundation/Foundation.h>
#import "BallMovingHandler.h"

@interface BallMovingHandler()
    @property (nonatomic, strong) NSTimer *timer;

    @property (nonatomic, assign) CGFloat deltaX;
    @property (nonatomic, assign) CGFloat deltaY;
@end

@implementation BallMovingHandler

-(instancetype) initWithBall: (Ball *) ball andUserBarrier: (Barrier *) userBarrier andEnemyBarrier: (Barrier *) enemyBarrier
{
    self = [self init];
    
    if (self)
    {
        self.ball = ball;
        self.enemyBarrier = enemyBarrier;
        self.userBarrier = userBarrier;

        self.deltaX = 1;
        self.deltaY = 1;
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 0.01 target: self selector: @selector(onTimer) userInfo: nil repeats: YES];
    }
    
    return self;
}

-(void) onTimer
{
    [self processBorderCollisions];
    
    CGPoint center = self.ball.center;
    
    if ([self.enemyBarrier hasCollisionWithBall: self.ball])
    {
        self.deltaX = -self.deltaX;
        self.deltaY = -self.deltaY;
        
        self.ball.center = CGPointMake(center.x + _deltaX, center.y + _deltaY + 15);
    }
    else if ([self.userBarrier hasCollisionWithBall: self.ball])
    {
        self.deltaX = -self.deltaX;
        self.deltaY = -self.deltaY;
        
        self.ball.center = CGPointMake(center.x + _deltaX, center.y + _deltaY - 15);
    }
    else
    {
        self.ball.center = CGPointMake(center.x + _deltaX, center.y + _deltaY);
    }

    if (self.ball.center.y <= self.enemyBarrier.center.y)
    {
        [self.delegate ballPassedThroughEnemyBarrier];
    }

    if (self.ball.center.y >= self.userBarrier.center.y)
    {
        [self.delegate ballPassedThroughUserBarrier];
    }
}

-(void) processBorderCollisions
{
    if (self.ball.frame.origin.x + self.ball.frame.size.width >= UIScreen.mainScreen.bounds.size.width)
    {
        // right
        self.deltaX = -self.deltaX;
    }
    if (self.ball.frame.origin.x <= UIScreen.mainScreen.bounds.origin.x)
    {
        // left
        self.deltaX = -self.deltaX;
    }
    
    if (self.ball.frame.origin.y <= UIScreen.mainScreen.bounds.origin.y)
    {
        // top
        self.deltaY = -self.deltaY;
    }
    
    if (self.ball.frame.origin.y + self.ball.frame.size.height >= UIScreen.mainScreen.bounds.size.height)
    {
        // bottom
        self.deltaY = -self.deltaY;
    }
}

-(void) dealloc
{
    [self stopMoving];
}

-(void) stopMoving
{
    [self.timer invalidate];
    self.timer = nil;
    self.delegate = nil;
}

@end
