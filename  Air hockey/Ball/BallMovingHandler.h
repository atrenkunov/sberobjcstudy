//
//  BallMovingHandler.h
//  trenkunov_education
//
//  Created by apple on 17/06/2019.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ball.h"
#import "Barrier.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BallPassedThroughBarrierNotifier
-(void) ballPassedThroughUserBarrier;
-(void) ballPassedThroughEnemyBarrier;
@end

@interface BallMovingHandler : NSObject

@property (nonatomic, weak) Ball *ball;

@property (nonatomic, weak) Barrier *enemyBarrier;
@property (nonatomic, weak) Barrier *userBarrier;

-(instancetype) initWithBall: (Ball *) ball andUserBarrier: (Barrier *) userBarrier andEnemyBarrier: (Barrier *) enemyBarrier;
-(void) stopMoving;

@property (nonatomic, weak) id<BallPassedThroughBarrierNotifier> delegate;

@end

NS_ASSUME_NONNULL_END
