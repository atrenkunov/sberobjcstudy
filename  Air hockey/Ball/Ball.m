//
//  Ball.m
//  trenkunov_education
//
//  Created by apple on 17/06/2019.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import "Ball.h"

@interface Ball()
    @property (nonatomic, strong) UIColor *color;
@end

@implementation Ball

- (instancetype) initWithRadius: (CGFloat) radius andColor: (UIColor *) color
{
    self = [super initWithFrame: CGRectMake(0, 0, radius * 2, radius * 2)];

    if (self)
    {
        self.radius = radius;
        self.color = color;
        
        self.backgroundColor = self.color;
        self.layer.cornerRadius = _radius;
    }
    
    return self;
}

-(BOOL) containsPoint: (CGPoint) point
{
    CGPoint center = self.center;
    
    return (center.x - point.x) * (center.x - point.x) +
           (center.y - point.y) * (center.y - point.y) <= _radius * _radius;
}

@end
