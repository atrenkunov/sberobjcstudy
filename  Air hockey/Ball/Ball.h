//
//  Ball.h
//  trenkunov_education
//
//  Created by apple on 17/06/2019.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Ball : UIView

@property (nonatomic, assign) CGFloat radius;

- (instancetype) initWithRadius: (CGFloat) radius andColor: (UIColor *) color;
-(BOOL) containsPoint: (CGPoint) point;

@end

NS_ASSUME_NONNULL_END
