//
//  main.m
//  trenkunov_education
//
//  Created by admin on 27.05.19.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
