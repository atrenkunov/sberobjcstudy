//
//  ViewController.m
//  trenkunov_education
//
//  Created by admin on 27.05.19.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import "CustomTabbarController.h"
#include "SingleBallController/SingleBallController.h"

@interface CustomTabbarController ()

@end

@implementation CustomTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"viewDidLoad started");
    
    [self setUpTabbar];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) setUpTabbar
{
    SingleBallController * singleBallController = [[SingleBallController alloc] initWithRadius: 35.0 andColor: [UIColor greenColor]];
    [self setViewControllers: @[singleBallController]];
}

@end
