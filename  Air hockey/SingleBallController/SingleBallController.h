//
//  SingleBallControllerViewController.h
//  trenkunov_education
//
//  Created by admin on 27.05.19.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BallMovingHandler.h"

@interface SingleBallController : UIViewController <BallPassedThroughBarrierNotifier>

- (instancetype) initWithRadius: (CGFloat) radius andColor: (UIColor *) color;

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event;

@end
