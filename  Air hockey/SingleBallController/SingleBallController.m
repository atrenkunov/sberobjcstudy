//
//  SingleBallControllerViewController.m
//  trenkunov_education
//
//  Created by admin on 27.05.19.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import "SingleBallController.h"

#import "BarrierMovingHandler.h"
#import "Ball.h"
#import "Barrier.h"

@interface SingleBallController()

@property (nonatomic, strong) Ball *ball;
@property (nonatomic, strong) BallMovingHandler *ballMovingHandler;

@property (nonatomic, strong) Barrier *enemyBarrier;
@property (nonatomic, strong) Barrier *userBarrier;

@property (nonatomic, strong) UILabel *score;

@property (nonatomic, strong) NSMutableArray<BarrierMovingHandler *> *barrierMovingHandlers;

@property (nonatomic, assign) BOOL trackingBegan;
@property (nonatomic, assign) CGFloat offsetFromCenterX;

@property(nonatomic, assign) NSInteger userScore;
@property(nonatomic, assign) NSInteger enemyScore;

@end

@implementation SingleBallController

- (instancetype) initWithRadius: (CGFloat) radius andColor: (UIColor *) color
{
    self = [super init];
    
    if (self)
    {
        self.userScore = 0;
        self.enemyScore = 0;

        self.trackingBegan = NO;
        self.offsetFromCenterX = 0.0;

        self.score = [[UILabel alloc] initWithFrame: CGRectMake(15, 10, 300, 40)];
        [self.score setFont: [UIFont boldSystemFontOfSize: 20]];

        self.ball = [[Ball alloc] initWithRadius: radius andColor: color];
        [self passBallIntoCenter];

        self.enemyBarrier = [[Barrier alloc] initWithLenght: 150 andHeight:20 andColor: [UIColor redColor]];
        [self.enemyBarrier moveToPoint: CGPointMake(50, 50)];
        
        self.userBarrier = [[Barrier alloc] initWithLenght: 100 andHeight:20 andColor: [UIColor purpleColor]];
        self.userBarrier.center = CGPointMake(UIScreen.mainScreen.bounds.size.width / 2, UIScreen.mainScreen.bounds.size.height - 100);

        self.barrierMovingHandlers = [NSMutableArray new];
        [self.barrierMovingHandlers addObject: [[BarrierMovingHandler alloc] initWithBarrier: self.enemyBarrier andSpeed: 7]];
        
        self.ballMovingHandler = [[BallMovingHandler alloc] initWithBall: self.ball andUserBarrier: _userBarrier andEnemyBarrier: _enemyBarrier];
        self.ballMovingHandler.delegate = self;
    }
    
    return self;
}

-(void) passBallIntoCenter
{
    self.ball.center = CGPointMake(UIScreen.mainScreen.bounds.size.width / 2, UIScreen.mainScreen.bounds.size.height / 2);
}

-(void) dealloc
{
}
                                 
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tabBarItem.title = @"Single ball";

    [self updateScoreLabel];

    [self.view addSubview: self.ball];
    [self.view addSubview: self.enemyBarrier];
    [self.view addSubview: self.userBarrier];
    [self.view addSubview: self.score];
}

-(void) updateScoreLabel
{
    [self.score setText: [NSString stringWithFormat: @"SCORE %@   :   %@", @(_userScore), @(_enemyScore)] ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    NSLog(@"touchesBegan");
    
    CGPoint pos = [touches.allObjects.firstObject locationInView: self.view];
    
    NSLog(@"x = %@ and y = %@", @(pos.x), @(pos.y));

    if (CGRectContainsPoint(self.userBarrier.frame, pos))
    {
        self.trackingBegan = YES;
        self.offsetFromCenterX = self.userBarrier.center.x - pos.x;
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    NSLog(@"touchesMoved");

    CGPoint pos = [touches.allObjects.firstObject locationInView: self.view];
    
    if (self.trackingBegan)
    {
        self.userBarrier.center = CGPointMake(pos.x + self.offsetFromCenterX, self.userBarrier.center.y);
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    NSLog(@"touchesEnded");
    self.trackingBegan = NO;
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    NSLog(@"touchesCancelled");
    self.trackingBegan = NO;
}

- (BOOL) ballContainsCursor: (CGPoint) cursor
{
    if ( pow(cursor.x - self.ball.center.x, 2.0) +
         pow(cursor.y - self.ball.center.y, 2.0) <= pow(self.ball.radius, 2.0))
    {
        NSLog(@"Inside ball");
        return YES;
    }
    
    NSLog(@"Outside ball");
    
    return NO;
}

-(void) ballPassedThroughUserBarrier
{
    NSLog(@"ballPassedThroughUserBarrier");
    
    self.userScore += 1;
    [self updateScoreLabel];
    
    if (self.userScore == 3)
    {
        [self stopGame];
        return;
    }

    [self passBallIntoCenter];
}


-(void) ballPassedThroughEnemyBarrier
{
    NSLog(@"ballPassedThroughEnemyBarrier");
    
    self.enemyScore += 1;
    [self updateScoreLabel];

    if (self.enemyScore == 3)
    {
        [self stopGame];
        return;
    }
    
    [self passBallIntoCenter];
}

-(void) stopGame
{
    [self.ballMovingHandler stopMoving];
    self.ballMovingHandler = nil;

    for (BarrierMovingHandler *handler in self.barrierMovingHandlers) {
        [handler stopMoving];
    }
    self.barrierMovingHandlers = nil;
    
    [self.ball removeFromSuperview];
    [self.userBarrier removeFromSuperview];
    [self.enemyBarrier removeFromSuperview];

    [UIView animateWithDuration: 5 animations:^() {
        self.score.frame = CGRectMake(70, 300, UIScreen.mainScreen.bounds.size.width, 40);
        [self.score setFont: [UIFont boldSystemFontOfSize: 40]];;
    } completion: ^(BOOL finished){
        if (self.userScore == 3)
        {
            [self.score setText: @"YOU LOOSE!"];
        }
        else
        {
            [self.score setText: @"YOU WIN!"];
        }
    }];
}

@end
