//
//  AppDelegate.h
//  trenkunov_education
//
//  Created by admin on 27.05.19.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

