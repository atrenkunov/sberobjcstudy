//
//  BarrierMovingHandler.m
//  trenkunov_education
//
//  Created by apple on 17/06/2019.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import "BarrierMovingHandler.h"
#import "Barrier.h"

@interface BarrierMovingHandler()

@property (nonatomic, weak) Barrier *barrier;
@property (nonatomic, assign) CGFloat speed;

@property (nonatomic, assign) NSInteger deltaX;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation BarrierMovingHandler

-(instancetype) initWithBarrier: (Barrier *) barrier andSpeed: (CGFloat) speed
{
    self = [super init];
    
    if (self)
    {
        self.deltaX = 2;
        self.barrier = barrier;
        self.speed = speed;
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 / (speed * 10) target: self selector: @selector(onTimer) userInfo: nil repeats: YES];
    }

    return self;
}

-(void) onTimer
{
    NSInteger minOffset = 5;
    
    CGFloat rightCorner = self.barrier.length + self.barrier.frame.origin.x;
    CGFloat leftCorner = self.barrier.frame.origin.x;
    
    if (rightCorner + 5 >= [UIScreen mainScreen].bounds.size.width)
    {
        self.deltaX = -self.deltaX;
    }

    if (leftCorner <= minOffset)
    {
        self.deltaX = -self.deltaX;
    }
    
    self.barrier.center = CGPointMake(self.barrier.center.x + self.deltaX, self.barrier.center.y);
}

-(void) dealloc
{
    [self stopMoving];
}

-(void) stopMoving
{
    [self.timer invalidate];
    self.timer = nil;
}

@end
