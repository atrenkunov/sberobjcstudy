//
//  Barrier.h
//  trenkunov_education
//
//  Created by apple on 17/06/2019.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ball.h"

NS_ASSUME_NONNULL_BEGIN

@interface Barrier : UIView

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, assign) NSInteger length;
@property (nonatomic, assign) NSInteger height;

- (instancetype) initWithLenght: (NSInteger) lenght andHeight: (NSInteger) height andColor: (UIColor *) color;

- (void) moveToPoint: (CGPoint) point;

- (BOOL) hasCollisionWithBall: (Ball *) ball;

@end

NS_ASSUME_NONNULL_END
