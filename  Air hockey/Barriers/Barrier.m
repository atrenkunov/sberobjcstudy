

#import "Barrier.h"

@implementation Barrier

- (instancetype) initWithLenght: (NSInteger) lenght andHeight: (NSInteger) height andColor: (UIColor *) color;
{
    self = [super initWithFrame: CGRectMake(0, 0, lenght, height)];
    
    if (self)
    {
        self.color = color;
        self.length = lenght;
        self.height = height;
        
        self.backgroundColor = self.color;
    }
    
    return self;
}

- (void) moveToPoint: (CGPoint) point
{
    CGRect currentFrame = self.frame;
    currentFrame.origin = point;

    self.frame = currentFrame;
}

- (BOOL) hasCollisionWithBall: (Ball *) ball
{
    if ([ball containsPoint: CGPointMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame))] ||
        [ball containsPoint: CGPointMake(CGRectGetMinX(self.frame), CGRectGetMaxY(self.frame))] ||
        [ball containsPoint: CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))] ||
        [ball containsPoint: CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMinY(self.frame))])
    {
        return true;
    }

    CGFloat circleDistanceX = fabs(ball.center.x - self.frame.origin.x - self.length / 2);
    CGFloat circleDistanceY = fabs(ball.center.y - self.frame.origin.y - self.height / 2);
        
    if (circleDistanceX > (self.length/2 + ball.radius)) { return false; }
    if (circleDistanceY > (self.height/2 + ball.radius)) { return false; }
        
    if (circleDistanceX <= (self.length /2)) { return true; }
    if (circleDistanceY <= (self.height/2)) { return true; }

    CGFloat cornerDistance_sq = (circleDistanceX - self.length /2) * (circleDistanceX - self.length /2) +
                                (circleDistanceY - self.height/2) * (circleDistanceY - self.height/2);

    return (cornerDistance_sq <= ball.radius * ball.radius);
}

@end
