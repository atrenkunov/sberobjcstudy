//
//  BarrierMovingHandler.h
//  trenkunov_education
//
//  Created by apple on 17/06/2019.
//  Copyright © 2019 sberbankUniversity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Barrier.h"

NS_ASSUME_NONNULL_BEGIN

@interface BarrierMovingHandler : NSObject

-(instancetype) initWithBarrier: (Barrier *) barrier andSpeed: (CGFloat) speed;
-(void) stopMoving;

@end

NS_ASSUME_NONNULL_END
